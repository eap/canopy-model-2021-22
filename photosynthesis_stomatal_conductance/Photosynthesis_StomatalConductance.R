source(here::here("utility/calc_leafboundaryconductance.R"))
source(here::here("utility/setup_parameters.R"))

pars

# 1 loading libraries ----
library(tidyverse)
########################################

# 2 reading input data ----
fluxes <- read.csv("Measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv")

meteo <- read.csv("Measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv")

Ta <- meteo$TA_degC + 273.15 # Air temperature in degree Celsius


# 3 setting parameters ----

## 3.1 stomata conductance ----
#foliage_water_potential. soil moisture/root water uptake?# saturated soils
minimum_conductance <- 0.01                  # g0 in mol H2O m-2 s-1
slope <- 9                                   # in mol H2O m-2 s-1
C_a <- fluxes$CO2conc_ppm                    # carbon conc of ambient air in umol mol-1
u <- meteo$WS_ms.1                           # 44m Wind_speed (m/s): 0.1 -> 10 m/s by 0.1?
l <- 0.05                                    # leaf_size 0.05 in meters 
Tl <- Ta                                     # trash temperature (20°C) in kelvin.
Ds <- meteo$VPD_hPa                          # vapor pressure deficit at the leaf surface in hPa



## 3.2 for photosynthesis ----
o_i <- pars$o2_atm /1000               # atmospheric oxygen concentration in ppm, convert to mmol mol-1
c_i <- (fluxes$CO2conc_ppm * 0.7)      # value in umol cO2 mol-1, we take 0.7 * concentration at ambient air
### table 12.4 use formula second to last to calibrate c_i according to An and gsw
universal_gas <- 8.314                 # J K-1 mol-1
leaf_absorptance <- 0.85              # leaf absorptance can be set to 0.85 according to table 14.1 for broadleaf forests. according to page 172 though (chapter11.2) is leaf_a commonly 0.8
f_spectralquality <- 0.30
### replace temperature and fluxes$ by stomata's variable names


## for the following three parameters, divide by air pressure (Pa) instead of by 100 to be more precise
## table 11.1
pressure_pa <- meteo$PRESS_hPa * 100             # air pressure in pascal
Kc25 <- 40.4 / pressure_pa * 10 ^ 6              # umol mol-1 from caemmerer 1994 and 2000
Ko25 <-  24800 / pressure_pa * 10 ^ 3            # mmol mol-1 from caemmerer 1994 and 2000
gamma_star25 <- 3.69 / pressure_pa * 10 ^ 6      # umol mol-1 from caemmerer 1994 and 2000

# page 175, chapter 11.3, table 11.4
vc_max25 <- 57.7                # broadleaf deciduous (beech) tree. umol m-2 s-1
Jmax25 <- 1.67 * vc_max25
PAR <- meteo$PPFD_IN_umolm.2s.1        # incident light (incoming photosynthetic photonflux density) in umol m-2 s-1



# 4 defining functions ----
## 4.1 for photosynthesis ----
## 4.1.1 electron transport rate ----
## 11.2 rate of carboxylation by rubisco
carbox_rate_vc <- function() {
  carboxilation_rate <- (vc_max * c_i) / (c_i + (Kc * (1 + (o_i / Ko))))
}

## electron transport rate 11.20
J_static <- function() {
  electron_transport_rate <- (4.5 + 10.5 * gamma_star / c_i) * (carbox_rate_vc())
}
# boxplot(J_static())

## amount of light utilized by photosystem II 11.22
### page 170 says f_spec is 0.15
I_PS2 <- function() {
  lightby_IPS2 <- ((1 - f_spectralquality) / (2)) * leaf_absorptance * PAR   ## leaf absorptance can be set to 0.85 according to table 14.1 for broadleaf forests 
  
}


## delta J function 11.21
delta_j <- function() {
  curvature_parameter <- ((I_PS2() + Jmax) * J_static() - I_PS2() * Jmax) / (J_static() ^ 2)
}


## electron transport rate J 11.24
electron_transport_J <- function() {
  J_wsolarrad <- ((I_PS2() + Jmax) - ((I_PS2() + Jmax)^2 - (4 * delta_j() * I_PS2() * Jmax)) ^ (1/2)) / (2 * delta_j())
}

## 4.1.2 limits to photosynthesis ----

## rubisco limited assimilation (function nr. 171 page 11.28)
# i think umol equals ppm (the units of pars$o2_atm are in ppm), but we need actually mmol, so we divide by 100
rubiscolim_function <- function() {
  result_rubisco <- (vc_max * (c_i - gamma_star)) / (c_i + Kc * (1 + (o_i / Ko)) )
  return(result_rubisco)
}


## light limited assimilation (function nr. 171 page 11.29)
lightlim_function <- function() {
  result_lightlim <- (electron_transport_J() / 4) * ((c_i - gamma_star) / (c_i + 2 * gamma_star))
  return(result_lightlim)
}


# photosynthesis-process choice
## (function nr. 11.30 page 171)
## also called gross-photosynthesis
gross_photosynthesis <- function() {
  result_limphot <- apply(data.frame(lightlim_function(), rubiscolim_function()), 1, FUN = min)
  return(result_limphot)
}

## 4.1.3 respiration ----
respiration <- function() {
  resp_result <- 0.015 * vc_max25 * arrhenius("respiration") * temp_inhib("respiration")
}


## 4.1.4 temperature limitations ----
## arrhenius function
### the arrhenius function changes its value of (delta)Ha according to the parameter it is being used for
### for clarifying this, see table 11.2 in the text book

### jmol-1
arrhenius <- function(target_variable = "Kc") {
  activation_energy <- switch(target_variable, 
                              "Kc" = 59356,
                              "Ko" = 35948,
                              "gamma_star" = 23400,
                              "vc_max" = 58520,
                              "respiration" = 66405,
                              "Jmax" = 37000
  )
  
  arrhenius_mult <- exp((activation_energy / (298.15 * universal_gas))  * (1 - 298.15 / Tl))
}


## temperature inhibition function
### the temperature inhibition function shows the thermal of breakdown biochemical processes
### For clarification, see Bonan book, Chapter 11, page 173, table 11.3
### units  jmol-1, jk-1 mol-1, 

temp_inhib <- function(target_variable = "vc_max") {
  deactivation_energy <- switch(target_variable,
                                "vc_max" = 149250,
                                "Jmax" = 152040,
                                "respiration" = 150000 # value from page 174 figure 3-description: PAREMETRIZATION HERE
  )
  
  entropy_term <- switch(target_variable,
                         "vc_max" = 485,
                         "Jmax" = 495,
                         "respiration" = 490 # value from page 174 figure 3-description. PAREMETRIZATION HERE
  )
  
  temp_inhib_multipl <- (1 + exp((298.15 * entropy_term - deactivation_energy) / (298.15 * (universal_gas)))) / (1 + exp((entropy_term * (Tl) - deactivation_energy) / (universal_gas * (Tl))))
  
}


## 4.1.5 net photosynthesis ----
net_photosynthesis <- function() {
  gross_photosynthesis() - respiration()
}

## 4.2 update values ----
### update values according to temperature
vc_max <- vc_max25 * arrhenius("vc_max") * temp_inhib("vc_max")
gamma_star <- gamma_star25 * arrhenius("gamma_star")
Ko <- Ko25 * arrhenius("Ko")
Kc <- Kc25 * arrhenius("Kc")
Jmax <- Jmax25 * arrhenius("Jmax") * temp_inhib("Jmax")

## 4.3 for stomatal conductance ----
##AKl##
gbw <- double()
for (i in 1:length(Ta)){
  boundcond <- calc_gb(Ta[i], pressure_pa[i], u[i], Tl[i],l)   # boundary layer conductance (gbh,gbw,gbc)
  gbw[i] <- boundcond$gbw
}
            
An <- net_photosynthesis() * 0.000001               # from sub-sub model (Santiago) converted into mol
carbon_conc_ls <- (C_a/10^6) - (1.4 / gbw) * An     # C_s calculating using equation 12.2 in mol mol-1
source(here::here("utility/canopy_parameters.R"))   # saturated vapor pressure
e_sat <- get_es(Tl - 273.15)
Fractional_Hummidity <- 1 - (Ds / e_sat)            # 42m above the forest h_s in mol m^2 s^1 and 0.223 = pressure of 1013.25 hPa and temp 15?C
                                                    # this needs to be calculates using equation 12.18

# implementing equations 12.14
# BallBerry model for stomatal conductance
stomata_conductance <- function(){
  result_stomata_cond <- minimum_conductance + (slope * (An / carbon_conc_ls) * Fractional_Hummidity)
}

# 5 actual script (running functions) ----

stomata_conductance() 
gross_photosynthesis()
net_photosynthesis()

# 6 writing output ----
### prepare output

output_StomataCond <- stomata_conductance() 
output_GPP <- gross_photosynthesis()
output_NPP <- net_photosynthesis()
output_R <- respiration()

# 7 plot figures ----
### write plots of what? our estimated values compared with measurements

## output
# 1. gross photosynthesis ~ time
library(ggforce)
library(lubridate)

fluxes <- c("Measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv") %>% 
  here::here() %>% 
  read_csv(show_col_types = FALSE)

gpp <- fluxes %>% 
  select(TIMESTAMP_END, meas =`GPP_DT_umolm-2s-1`) %>%
  mutate(meas = meas * - 1)

gpp$TIMESTAMP_END
gpp$mod <- output_GPP    ##AKl##

FIGURE <- gpp %>%
  summarise(xendd = mean(TIMESTAMP_END),
            x0 = (mean(TIMESTAMP_END) %m+% months(9)))

FIGURE2 <- gpp %>%
  summarise(xendd = mean(TIMESTAMP_END %m+% months(6)),
            x0 = (mean(TIMESTAMP_END) %m+% months(9)))

annotation1 <- geom_text(aes(x = FIGURE$x0 %m+% months(2), y = 40,
                             label = "overestimated LAI"),
                         stat = "unique", family = "Courier",
                         size = 2.5, color = "grey30")

annotation2 <- geom_text(aes(x = FIGURE$x0 %m+% months(2), y = 33,
                             label = "underestimated LAI"),
                         stat = "unique", family = "Courier",
                         size = 2.5, color = "grey30")

plt_timeseries <- ggplot(data=gpp, aes(x=TIMESTAMP_END)) +
  geom_line(aes(y=mod, color="simulations", alpha = 0.5)) +
  geom_line(aes(y=meas, color="observations", alpha = 0.5)) +
  # geom_hline(yintercept=0, linetype="dashed", color="black") +
  scale_x_datetime(date_breaks="2 months", date_labels="%b\n%Y") +
  xlab('Time') +
  ylab(expression("GPP ("*mu* "mol CO"[2]*" m"^-2*" s"^-1*")")) +
  geom_curve(data = FIGURE, inherit.aes = FALSE,
             aes(x = x0, y = 40, xend = xendd, yend = 12),
             size = 0.5, color = "grey30", alpha = 0.6,
             angle= 100,
             curvature = 0.4,
             arrow = arrow(length = unit(0.03, "npc"))) +
  geom_curve(data = FIGURE2, inherit.aes = FALSE,
             aes(x = x0, y = 33, xend = xendd, yend = 20),
             size = 0.5, color = "grey30", alpha = 0.6,
             angle= 800,
             curvature = 0.35,
             arrow = arrow(length = unit(0.03, "npc"))) +
  annotation1 +
  annotation2 +
  scale_color_manual(name="", values=c("darkblue", "darkred")) +
  #theme_classic()
  scale_alpha(guide = "none")  +
  # scale_x_continuous(expand = c(0, 0)) +
  scale_y_continuous(expand = c(0, 0)) +
  coord_cartesian(clip = "off") +
  theme_minimal() +
  theme(panel.grid = element_blank(),
        legend.position = c(.1, .85),
        legend.background = element_rect(colour = "transparent"),
        axis.title.x = element_text(vjust = 0, size = 12),
        axis.title.y = element_text(vjust = 2, size = 12),
        axis.text = element_text(color = "black"),
        axis.line = element_line(color = "black"),
        axis.ticks = element_line(color = "black"),
        panel.border = element_blank())
plt_timeseries
ggsave("photosynthesis_stomatal_conductance/Plots/GPP_timeseries.png", width=10, height=6, dpi=300)


# 2. GPP meas vs mod
y_min <- min(gpp$meas, gpp$mod)
y_max <- max(gpp$meas, gpp$mod)
gpp$quarter <- quarter(gpp$TIMESTAMP_END)


ggplot(data=gpp) +
  geom_point(aes(x=meas, y=mod),
             size = 2.5,
             colour='white',
             fill = "#d86b1d",
             shape = 21,
             alpha=.6) +
  geom_smooth(aes(x=meas, y=mod), method="lm", fullrange=TRUE, color="#ba5a14",
              size = 0.9,
              se = FALSE) + # adding regression line
  geom_hline(yintercept=0, linetype="dashed", color="black") +
  geom_vline(xintercept=0, linetype="dashed", color="black") +
  geom_abline(slope=1, linetype="dashed", color="black") +
  xlab(expression("Measured GPP ("*mu* "mol CO"[2]*" m"^-2*" s"^-1*")")) +
  ylab(expression("Simulated GPP ("*mu* "mol CO"[2]*" m"^-2*" s"^-1*")")) +
  xlim(y_min - 0.1*y_min, y_max + 0.1*y_max) +
  ylim(y_min - 0.1*y_min, 16) +
  # scale_size(range = c(0.2, 4)) +
  theme_light() +
  theme(legend.position="none",
        panel.grid = element_blank(),
        axis.title.x = element_text(vjust = 0, size = 12),
        axis.title.y = element_text(vjust = 2, size = 12),
        axis.text = element_text(color = "black"),
        axis.line = element_line(color = "black"),
        axis.ticks = element_line(color = "black"),
        panel.border = element_blank()) 
# +
  # coord_fixed()
ggsave("photosynthesis_stomatal_conductance/Plots/GGP_modvmeas2.png", width=8, height=5, dpi=300)



# 3. gross photos ~ PAR
head(gpp)
length(gpp$TIMESTAMP_END)
length(PAR)
gpp$photar <- PAR

ggplot(data=gpp) +
  geom_point(aes(x=photar, y=mod), size=0.7, color='grey30', alpha=.5) +
  geom_point(aes(x=photar, y=meas), size=0.7, color='blue', alpha=.1) +
  xlab(expression("Photon flux density ("*mu* "mol m"^-2*" s"^-1*")")) +
  ylab(expression("Modelled GPP ("*mu* "mol CO"[2]*" m"^-2*" s"^-1*")")) +
  scale_x_continuous(expand = c(0, 0)) +
  scale_y_continuous(expand = c(0, 0)) +
  coord_cartesian(clip = "off") +
  theme_light() +
  theme(legend.position="none",
        panel.grid = element_blank(),
        axis.title.x = element_text(vjust = 0, size = 12),
        axis.title.y = element_text(vjust = 2, size = 12),
        axis.text = element_text(color = "black"),
        axis.line = element_line(color = "black"),
        axis.ticks = element_line(color = "black"),
        panel.border = element_blank())
ggsave("photosynthesis_stomatal_conductance/Plots/GGPvsPAR.png", width=6, height=5, dpi=300)

# 4. gross photos ~ temp
gpp$temp <- meteo$TA_degC

ggplot(data=gpp) +
  geom_point(aes(x=temp, y=mod), size=0.7, color='grey30', alpha=.3) +
  geom_point(aes(x=temp, y=meas), size=0.7, color='blue', alpha=.1) +
  xlab(expression("Temperature (°C)")) +
  ylab(expression("GPP ("*mu* "mol CO"[2]*" m"^-2*" s"^-1*")")) +
  scale_x_continuous(expand = c(0, 0)) +
  scale_y_continuous(expand = c(0, 0)) +
  coord_cartesian(clip = "off") +
  theme_light() +
  theme(legend.position="none",
        panel.grid = element_blank(),
        axis.title.x = element_text(vjust = 0, size = 12),
        axis.title.y = element_text(vjust = 2, size = 12),
        axis.text = element_text(color = "black"),
        axis.line = element_line(color = "black"),
        axis.ticks = element_line(color = "black"),
        panel.border = element_blank())
ggsave("photosynthesis_stomatal_conductance/Plots/GGPvstemp.png", width=6,
       height=5, dpi=300)

gpp$carbon_conc <- C_a

ggplot(data=gpp) +
  geom_point(aes(x=carbon_conc, y=mod), size=0.7, color='grey30', alpha=.5) +
  xlab(expression("Atmospheric CO"[2])) +
  ylab(expression("Modelled GPP ("*mu* "mol CO"[2]*" m"^-2*" s"^-1*")")) +
  scale_x_continuous(expand = c(0, 0)) +
  scale_y_continuous(expand = c(0, 0)) +
  coord_cartesian(clip = "off") +
  theme_light() +
  theme(legend.position="none",
        panel.grid = element_blank(),
        axis.title.x = element_text(vjust = 0, size = 12),
        axis.title.y = element_text(vjust = 2, size = 12),
        axis.text = element_text(color = "black"),
        axis.line = element_line(color = "black"),
        axis.ticks = element_line(color = "black"),
        panel.border = element_blank())
ggsave("photosynthesis_stomatal_conductance/Plots/GGPvcarbonconcentration.png", width=6,
       height=5, dpi=300)



# 5 GPP diurnal

diurnal <- gpp %>% 
  filter(month(TIMESTAMP_END) == 5 & year(TIMESTAMP_END) == 2017) %>% 
  summarize(meas_mean=mean(meas), meas_sd=sd(meas), mod_mean=mean(mod), mod_sd=sd(mod))


plt_shorttimeseries <- ggplot(data=diurnal, aes(x=TIMESTAMP_END, alpha = 0.5)) +
  geom_point(aes(y=mod, color="simulations")) +
  geom_line(aes(y=meas, color="observations")) +
  # geom_hline(yintercept=0, linetype="dashed", color="black") +
  scale_x_datetime(date_breaks="2 months", date_labels="%b\n%Y") +
  xlab('Time') +
  ylab(expression("GPP ("*mu* "mol CO"[2]*" m"^-2*" s"^-1*")")) +
  scale_color_manual(name="", values=c("darkblue", "darkred")) +
  #theme_classic()
  scale_alpha(guide = "none")  +
  # scale_x_continuous(expand = c(0, 0)) +
  scale_y_continuous(expand = c(0, 0)) +
  coord_cartesian(clip = "off") +
  theme_minimal() +
  theme(panel.grid = element_blank(),
        legend.position = c(.15, .85),
        legend.background = element_rect(colour = "transparent"),
        axis.title.x = element_text(vjust = 0, size = 12),
        axis.title.y = element_text(vjust = 2, size = 12),
        axis.text = element_text(color = "black"),
        axis.line = element_line(color = "black"),
        axis.ticks = element_line(color = "black"),
        panel.border = element_blank())
plt_shorttimeseries
# ggplot(data=diurnal, aes(x=hour)) +
#   # Model
#   geom_line(aes(y=mod_mean, color="simulations"), size=2) +
#   geom_errorbar(aes(ymin=(mod_mean - mod_sd),
#                     ymax=(mod_mean + mod_sd),
#                     color="simulations"),
#                 linetype  = "dashed",
#                 width = 0.1,
#                 position=position_dodge(width=0.5)) +
#   # Measurements
#   geom_line(aes(y=meas_mean, color="observations"), size=2) +
#   geom_errorbar(aes(ymin=(meas_mean - meas_sd),
#                     ymax=(meas_mean + meas_sd),
#                     color="observations"),
#                 linetype  = "dashed",
#                 width = 0.1,
#                 position=position_dodge(width=0.5)) +
#   # geom_hline(yintercept=0, linetype="dashed", color="black") +
#   xlab('time of day') +
#   ylab('mean variable name (unit)') +
#   scale_color_manual(name="", values = c("darkred", "darkblue")) +
#   theme_light()
# ggsave("plot_diurnal.png", width=6, height=5, dpi=300)

# 7.3 stomata conductance-----

library(tidyverse)
library(here)
library(ggplot2)
library(lubridate)
library(zoo)

soil_meteo <- c("Measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE)

meteo <- c("Measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE)
meteo$StomataCond <- output_StomataCond
meteo$output_NPP <- output_NPP
meteo$output_GPP<- output_GPP

#### Plot: stomata conductance for two years ####

plt_timeseries <- ggplot(data=meteo, aes(x=TIMESTAMP_END)) +
  geom_point(aes(y=StomataCond, color="simulations")) +
  geom_hline(yintercept=0, linetype="dashed", color="black") +
  scale_x_datetime(date_breaks="2 months", date_labels="%b\n%Y") +
  xlab('time') +
  ylab('stomata conductance (mol m^-2 s^-1)') +
  scale_color_manual(name="", values=c("darkblue")) +
  #theme_classic()
  theme_light()
plt_timeseries
ggsave("plot_timeseries_stomata.png", width=10, height=6, dpi=300)


meteo_short <- data.frame(StomataCond = meteo$StomataCond[(meteo$TIMESTAMP_END >= '2017-07-01') & (meteo$TIMESTAMP_END <= '2017-07-10')])
meteo_short$TIMESTAMP_END <- meteo$TIMESTAMP_END[(meteo$TIMESTAMP_END >= '2017-07-01') & (meteo$TIMESTAMP_END <= '2017-07-10')]
meteo_short$TA_degC <- meteo$TA_degC[(meteo$TIMESTAMP_END >= '2017-07-01') & (meteo$TIMESTAMP_END <= '2017-07-10')]
meteo_short$VPD_hpa <- meteo$VPD_hPa[(meteo$TIMESTAMP_END >= '2017-07-01') & (meteo$TIMESTAMP_END <= '2017-07-10')]
meteo_short$`SWC_32cm_%` <- soil_meteo$`SWC_32cm_%`[(soil_meteo$TIMESTAMP_END >= '2017-07-01') & (soil_meteo$TIMESTAMP_END <= '2017-07-10')]
meteo_short$`PPFD_IN_umolm-2s-1` <- meteo$`PPFD_IN_umolm-2s-1`[(meteo$TIMESTAMP_END >= '2017-07-01') & (meteo$TIMESTAMP_END <= '2017-07-10')]

##### Plot: stomata conductance for 10 summer days ######

plt_timeseries_short <- ggplot(data=meteo_short, aes(x=TIMESTAMP_END)) +
  geom_line(aes(y=StomataCond, color="simulations")) +
  geom_point(aes(y=StomataCond, color="data pionts")) +
  geom_hline(yintercept=0, linetype="dashed", color="black") +
  scale_x_datetime(date_breaks="12 hours", date_labels="%d-%m\n%H") +
  xlab('time') +
  ylab('stomata conductance (mol m^-2 s^-1)') +
  # scale_color_manual(name="", values=c("darkblue")) +
  # theme_classic()
  theme_light()
plt_timeseries_short
ggsave("plot_timeseries_stomata_short.png", width=10, height=6, dpi=300)

#### Plot: stomata conductance ~ gross_photosynthesis ####
ggplot(data=meteo) +
  geom_point(aes(x=StomataCond, y=output_GPP), size=1, color='blue', alpha=.5) +
  xlab('stomata conductance (mol m^-2 s^-1') +
  ylab('gross_photosynthesis(umolm^-2 s^-1))') +
  theme_light() 
#theme(aspect.ratio=1, legend.position="none")
ggsave("plot_scatter_GPP.png", width=6, height=5, dpi=300)

#### Plot: stomata conductance ~ air_temperature   ####
ggplot(data=meteo_short) +
  geom_point(aes(x=TA_degC, y=StomataCond), size=1, color='grey30', alpha=.5) +
  xlab('air temperature(°C)') +
  ylab('stomata conductance (mol m^-2 s^-1)') +
  theme_light() 
 # theme(aspect.ratio=1, legend.position="none")
ggsave("plot_scatter_Temp.png", width=6, height=5, dpi=300)

#### Plot: stomata conductance ~ vapour pressure deficit ####
ggplot(data=meteo_short) +
  geom_point(aes(x=VPD_hpa, y=StomataCond), size=1, color='purple', alpha=.5) +
  xlab('vapour pressure deficit (hpa)') +
  ylab('stomata conductance (mol m^-2 s^-1)') +
  theme_light() 
# theme(aspect.ratio=1, legend.position="none")
ggsave("plot_scatter_VPD.png", width=6, height=5, dpi=300)

#### Plot: stomata conductance ~ incoming photosynthetic photon flux density####
ggplot(data=meteo_short) +
  geom_point(aes(x=`PPFD_IN_umolm-2s-1`, y=StomataCond), size=1, color='blue', alpha=.5) +
  xlab('Photosynthetically active radiation(umol m^-2 s^-1)') +
  ylab('stomata conductance (mol m^-2 s^-1)') +
  theme_light() 
# theme(aspect.ratio=1, legend.position="none")
ggsave("plot_scatter_ppFD.png", width=6, height=5, dpi=300)

#### Plot: stomata conductance ~ volumetric soil water content ####
ggplot(data=meteo_short) +
  geom_point(aes(x=`SWC_32cm_%`, y=StomataCond), size=1, color='blue', alpha=.5) +
  xlab('volumetric soil water content(%)') +
  ylab('stomata conductance (mol m^-2 s^-1)') +
  theme_light() 
# theme(aspect.ratio=1, legend.position="none")
ggsave("plot_scatter_swc.png", width=6, height=5, dpi=300)


## Plot: Respiration against temperature ##
gpp$resp <- respiration() 

ggplot(data=gpp) +
  geom_point(aes(x=temp, y=resp), size=0.7, color='blue', alpha=.5) +
  xlab(expression("Temperature(°C)")) +
  ylab(expression("Modelled Respiration ("*mu* "mol CO"[2]*" m"^-2*" s"^-1*")")) +
  #scale_x_continuous(expand = c(0, 0)) +
  #scale_y_continuous(expand = c(0, 0)) +
  coord_cartesian(clip = "off") +
  theme_light() +
  theme(legend.position="none",
        panel.grid = element_blank(),
        axis.title.x = element_text(vjust = 0, size = 12),
        axis.title.y = element_text(vjust = 2, size = 12),
        axis.text = element_text(color = "black"),
        axis.line = element_line(color = "black"),
        axis.ticks = element_line(color = "black"),
        panel.border = element_blank())
ggsave("plot_scatter_resp.png", width=6, height=5, dpi=300)



## Plot: gpp against respiration##
ggplot(data=gpp) +
  geom_point(aes(x=resp, y=mod), size=0.7, color='blue', alpha=.5) +
  xlab(expression("Model Respiration ("*mu* "mol O"[2]*" m"^-2*" s"^-1*")")) +
  ylab(expression("Modelled GPP ("*mu* "mol CO"[2]*" m"^-2*" s"^-1*")")) +
  scale_x_continuous(expand = c(0, 0)) +
  scale_y_continuous(expand = c(0, 0)) +
  coord_cartesian(clip = "off") +
  theme_light() +
  theme(legend.position="none",
        panel.grid = element_blank(),
        axis.title.x = element_text(vjust = 0, size = 12),
        axis.title.y = element_text(vjust = 2, size = 12),
        axis.text = element_text(color = "black"),
        axis.line = element_line(color = "black"),
        axis.ticks = element_line(color = "black"),
        panel.border = element_blank())
ggsave("plot_scatter_gpp_resp.png", width=6, height=5, dpi=300)


# simulated leaf respiration/measured ecosystem respiration ~ time

fluxes <- c("Measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv") %>% 
  here::here() %>% 
  read_csv(show_col_types = FALSE)

reps <- fluxes %>% 
  select(TIMESTAMP_END, meas =`RECO_DT_umolm-2s-1`) %>%
  mutate(meas = meas)

reps$meas
reps$mod <- output_R    ##AKl##

plt_timeseries <- ggplot(data=reps, aes(x=TIMESTAMP_END)) +
  geom_line(aes(y=mod, color="simulations")) +
  geom_line(aes(y=meas, color="observations")) +
  geom_hline(yintercept=0, linetype="dashed", color="black") +
  scale_x_datetime(date_breaks="2 months", date_labels="%b\n%Y") +
  xlab('Time') +
  ylab('simulated leaf respiration') +
  scale_color_manual(name="", values=c("darkblue", "darkred")) +
  #theme_classic()
  theme_light()
plt_timeseries
ggsave("plot_timeseries_leaf_resp.png", width=10, height=6, dpi=300)











