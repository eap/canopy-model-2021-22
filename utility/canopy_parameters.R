# to use this file put at the top of your script
# source(here::here("utility/canopy_parameters.R"))

# Load libraries
source(here::here("utility/setup_parameters.R"))
library(lubridate)
library(purrr)
library(tibble)

# Leaf Area Index ---------

#' Simple model to get current Leaf Area Index (LAI)
#'
#' Use a simple model to get the LAI of the different day of the year.
#' It has 4 phases:
#'  - Winter: from leaf_fall_complete to leaf_out -> LAI = 0
#'  - Spring: from leaf_out to leaf_full -> linear growth from 0 to max_LAI
#'  - Summer: from leaf_full to leaf_fall -> LAI = max_LAI
#'  - Fall: from leaf_fall to leaf_fall_complete -> linear decrease from max_LAI to 0
#' 
#' The parameters are used from the global pars are:
#' 
#' -`max_LAI` max LAI value in the summer
#' 
#' -`leaf_out day` leaves start in spring
#' -`leaf_full day` leaves reach max LAI
#' -`leaf_fall day` leaves start to fall
#' -`leaf_fall_complete` day all leaves are fallen
#'
#'The 4 parameters  are the day of the year
#'#'
#' @param datetime a (or a vector of) datetime object
#' 
#' @return LAI Leaf Area Index value for the day of the year
#' 
#' @example
#' get_day_LAI(now()) # Get the LAI of the current moment
#' @example 
#' days <- as_datetime(c("2021-01-01", "2021-05-01", "2021-07-01", "2021-10-15", "2021-12-01"))
#' get_day_LAI(days)
get_day_LAI <- function(time) {
  
  stopifnot(all(is.POSIXct(time)))
  ##  --- This is some complex code to be able to support timesteps smaller than a day
  # convert the days of years in dates and the integer
  start_year <- floor_date(time, "year") #first day of the year
  pars_date <- pars %>% 
    select(leaf_out, leaf_full, leaf_fall, leaf_fall_complete) %>% 
    # convert days to seconds
    map_df(~as_datetime(.x * 86400, origin=start_year) %>% as.integer)
  time <- as.integer(time) # convert to integer for easy comparison
  ## ---
  
  case_when(
    # Winter LAI is 0
    time < pars_date$leaf_out ~ 0,
    # spring LAI has linear increase
    time >= pars_date$leaf_out & time < pars_date$leaf_full ~ {
      ndays <- pars_date$leaf_full - pars_date$leaf_out # n days of the transition
      pars$max_LAI * (time - pars_date$leaf_out) / ndays
    },
    # summer LAI is max
    time >= pars_date$leaf_full & time < pars_date$leaf_fall ~ pars$max_LAI,
    # Autumn LAI is decreasing
    time >= pars_date$leaf_fall & time < pars_date$leaf_fall_complete ~ {
      ndays <- pars_date$leaf_fall_complete - pars_date$leaf_fall # n days of the transition
      pars$max_LAI * (pars_date$leaf_fall_complete - time) / ndays
    },
    # winter again
    time >= pars$leaf_fall_complete ~ 0
  )
}


#' Canopy Profile of Leaf Area Density
#' 
#' This is the implementation in R of
#' equation 2.3 from Bonan (https://doi.org/10.1017/9781107339217.003), page 27
#' 
#' @param x a number between 0 and 1, that represents relative height in the canopy
#' @return value of Leaf Area Density  
canopy_LAI_profile <- function(x){
  (x ^ (pars$p -1) * (1-x)^(pars$q -1)) /
    beta(pars$p, pars$q)
}

#' Amount of LAI divided between the different layers of the canopy
#' This produces the vertical profile of LAI
#' 
#' @param LAI the total value of LAI
#' @param n_layers the number of layers LAI should be divided into
#' 
#' @return data frame with a column n_layer (1 is on the bottom)
#'   and a column LAI with the relative LAI for the layer
#'   
#'@example
#'get_LAI_layers(5, 10) # division in layers with a LAI of 5 and 10 layers
get_LAI_layers <- function(LAI, n_layers) {
  
  # divide the vertical profile to have the upper and lower points for n layers
  breaks <- seq(0,1, length.out=n_layers+1)
  lower_limit <- breaks[1:length(breaks)-1]
  upper_limit <- breaks[2:length(breaks)]
  
  # for each of the layer `integrate` `canopy_LAI_profile` with the given limits
  LAI_layers <- map2(lower_limit, upper_limit, integrate, f = canopy_LAI_profile) %>% 
    map(pluck, "value") %>% #extract only the `value` from the integral result
    as.double() # simplify list
  
  LAI_layers <- LAI_layers * LAI # scale to actual LAI 
  
  tibble(n_layer = seq(n_layers), LAI = LAI_layers)
}

# Air properties ----------

#' calculates the saturation vapour pressure, given the air temperature
#' TODO: document where this formula is coming from
#' @param t (air) temperature in **degree Celsius**
#' @return e_s saturation vapour pressure in `hPa`
get_es <-  function(t)  {
  6.1078 * exp((17.08085 * t) / (234.175 + t))
}

#' converts relative humidity to actual vapour pressure
#' @param rh relative humidity in percentage `%` (from 0 to 100)
#' @return ea actual vapour pressure in `hPa`
rh2ea <- function(rh, e_s) {
  rh/100 * e_s
}

#' Convert from Celsius degrees to Kelvin
#' @param c temperature in Celsius degrees
#' @return k the temperature in kelvin degrees
c2k <- function(c) c + 273.15

#' Convert from Kelvin to Celsius
#' @param k the temperature in kelvin degrees
#' @return c temperature in Celsius degrees
k2c <- function(k) k - 273.15



#' Specific heat capacity at constant pressure
#' - according to FAO 56 guidelines cp = 1013. J kg-1 K-1
#' - according to Oncley et al. 2007, Boundary-Layer Meteorol. 123, page 16, 
#' @param q (optional) absolute air humidity in `kg m-3`
#' @return `cp` air specific heat capacity at constant pressure in `J kg-1 K-1`
get_cp <- function(q=NULL){
  if (is_null(q)){
    cp <-  1.013e3
  }
  else{
    cpd <-  1006.   # specific heat of dry air at constant pressure (J kg-1 K-1)
    cp <-  cpd * (1 + 0.84*q)  
  }
  return(cp)
}


#' Latent heat of vaporization
#' - according to FAO 56 guidelines lambda = 2.45*10**6 Jkg-1
#' - according to Oncley et al. 2007, Boundary-Layer Meteorol. 123, page 16,
#' lambda can be dependent on air temperature
#' @param t_air (optional) the air temperature in degC
#' @return `lambda`, latent heat of vaporization in `J kg-1`
get_lambda <- function(t_air=NULL){
  if (is_null(t_air)){
    lambda <- 2.45e6
  }
  else{
    lambda <-  2.501e6 - 2361.*t_air 
  }
  return(lambda)
}