
### Examining 50 year pre calibration run to take pool start values
yrs50 <- read.csv("carbon50years.csv")
plot(yrs50$heterotrophic_respiration)
plot(yrs50$leaf_carbon)
plot(yrs50$wood_carbon)
plot(yrs50$root_carbon)
plot(yrs50$metabolic_litter)
plot(yrs50$structural_litter)
plot(yrs50$CWD)
plot(yrs50$FSOM)
plot(yrs50$SSOM)
plot(yrs50$PSOM)

# h_respx10 <- cmodel_results$heterotrophic_respiration*10
# plot(resp_c, type = "l")
# points(h_respx10, col = "blue", pch = "*")
# plot(soil_resp)
# head(resp_c)

combined_sim_meas <- cbind(cmodel_results$heterotrophic_respiration, resp_c)
# combined_sim_meas <- cbind(measurements$TIMESTAMP_END, combined_sim_meas)
# 


# ##Ploting uncalibrated versus measured data to start
no.na.resp <- na.omit(combined_sim_meas)
plot(no.na.resp$`cmodel_results$heterotrophic_respiration`~no.na.resp$resp_c)
abline(lm(no.na.resp$`cmodel_results$heterotrophic_respiration`~no.na.resp$resp_c), col = "red")
lm(no.na.resp$`cmodel_results$heterotrophic_respiration`~no.na.resp$resp_c) ### weak correlation

### Determining pre calibration RMSE
rmse <- sqrt(mean((no.na.resp$`cmodel_results$heterotrophic_respiration`) - no.na.resp$resp_c)**2)
rmse
rsq <- function (x, y) cor(x, y) ^ 2
uncal_r2 <- rsq(no.na.resp$resp_c, no.na.resp$`cmodel_results$heterotrophic_respiration`)
r2 <- NULL
### Function to determine objective criterion
library(nloptr) ##AKl##

obj_crit <- function(opt_pars){
  #print(opt_pars) ##AKl##
  pars <- mutate(pars, S1=abs(opt_pars[1]), S2=abs(opt_pars[2]), S3=abs(opt_pars[3]), 
                S4=abs(opt_pars[4]), S5=abs(opt_pars[5]), S6=abs(opt_pars[6]), 
                S7=abs(opt_pars[7]), S8=abs(opt_pars[8]), S9=abs(opt_pars[9])) ##AKl##
  print(pars[45:54]) ##AKl##
  
  carbon_model_output <- carbon_model(GPP, LAI, soil_temperature_5cm_k, pars) ##AKl##
  ##AKl## cmodel_results <- cbind(measurements$TIMESTAMP_END, carbon_model_output)
  
  # Comparison with observations
  sim <- cbind(measurements$TIMESTAMP_END, carbon_model_output)
  calibration_df <- cbind(resp_c, sim)
  rmse = sqrt(mean((calibration_df$heterotrophic_respiration - calibration_df$resp_c)**2, na.rm = T))   # g C/(kg soil)
  print(rmse) ##AKl##
  return(rmse)
  # r2 = rsq(no.na.resp$resp_c, no.na.resp$`cmodel_results$heterotrophic_respiration`)
  # return(r2)
  # print(r2)
}

# (calibration_df$heterotrophic_respiration - calibration_df$resp_c)
# (rmse = sqrt(mean((calibration_df$heterotrophic_respiration - calibration_df$resp_c*10)**2, na.rm = T)))

# run Nelder-Mead Simplex to minimize RMSE
mod_calib <- neldermead(c(pars$S1, pars$S2, pars$S3, pars$S4, pars$S5, pars$S6, pars$S7,pars$S8,pars$S9), obj_crit)  ##AKl##

S1 <- 0.475
S2 <- 0.0650 
S3 <- 7.40
S4 <- 11.6
S5 <- 11.5
S6 <- 9.35
S7 <- 0.000483
S8 <- 5.77
S9 <- 8.30

                        
