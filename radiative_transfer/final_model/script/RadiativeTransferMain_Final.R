## 3 ##

## Main calculation ## 

# library needed 
library(here) # to deal with annoying file path
library(lubridate) # to deal with date data
library(tidyverse) 


## parameters and input ## ----

# global parameters, temporarily from our file 
# see the csv file for the details of the parameter 
pars <- read.csv("radiative_transfer/initial_model/script/parameters_tmp.csv", header = TRUE, row.names = 2) 

# subset values for parameters 

# the parameters are categorized into related words
para_phenology <- c("leaf_out", "leaf_full", "leaf_fall", "leaf_fall_complete")
para_LAI <- c("max_LAI", "min_LAI", "p", "q")
para_canopy <- c("Omega", "N_vege_layers")
para_leaf <- c("K_b", "omega_l_SW", "eps_l")
para_constant <- c("sigma", "T_0")
para_ground <- c("i_soil", "rho_gd", "rho_gb", "eps_g")

# subset values 
tpars <- as.data.frame(t(pars))
para_all_tmp <- tpars %>% 
  select(para_phenology, para_LAI, para_canopy, para_leaf, para_constant, para_ground) %>% 
  filter(rownames(tpars) == "value")
# the values converted into character 
# therefore change them into numeric 
para_all <- as.data.frame( 
  t(apply(para_all_tmp[1, ], 2, as.numeric))
  ) 



# assign values 
leaf_out = para_all$leaf_out
leaf_full = para_all$leaf_full
leaf_fall = para_all$leaf_fall
leaf_fall_complete = para_all$leaf_fall_complete

max_LAI = para_all$max_LAI
min_LAI = para_all$min_LAI
p = para_all$p
q = para_all$q

Omega = para_all$Omega
N_veg_layers = para_all$N_vege_layers

K_b = para_all$K_b
eps_l = para_all$eps_l

i_soil = para_all$i_soil
rho_gd = para_all$rho_gd
rho_gb = para_all$rho_gb
eps_g = para_all$eps_g

sigma = para_all$sigma
T_0 = para_all$T_0


## Assumptions related parameters

# Model Layers
N_layers <- N_veg_layers + 1 # Total number of layers

i_bot_l <- i_soil + 1 # Index for bottom leaf layer
i_top_l <-  N_veg_layers + i_bot_l - 1 # Index for top leaf layer


# Longwave Parameters
rho_g <- 1 - eps_g #reflectance of ground
omega_l_LW <- 1 - eps_l #leaf scattering coefficient
rho_l_LW <- omega_l_LW # In this case all the scattering is reflected
tau_l_LW <- omega_l_LW - rho_l_LW # This will be 0


## Input from measurements ## ----


## Radiation
input_meteo <- c("Measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE)

input_soil <- c("Measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE)

# make vectors for forloop
I_outgoing_predictions <- numeric()
I_outgoing_data <- numeric()

L_outgoing_predictions <- numeric()
L_outgoing_data <- numeric()



## Main function ----


calculate_state <- function(time_start, time_end) {
  
  for (data_timestep in time_start:time_end){
    T_soil <- input_soil$'TS_2cm_degC'[data_timestep]
    T_air <- input_meteo$'TA_degC'[data_timestep]
    Timestamp <- match(input_meteo$TIMESTAMP_END[data_timestep], input_meteo$TIMESTAMP_END)
    time <- input_meteo$TIMESTAMP_END[data_timestep]
    LW_Inc <- input_meteo$'LW_IN_Wm-2'[data_timestep] # Incoming Atmospheric Longwave Radiation 
    SW_Inc_d <- input_meteo$"SW_IN_Wm-2"[data_timestep] - input_meteo$"SW_DIF_IN_Wm-2"[data_timestep] # Incoming Atmospheric Shortwave Diffuse Radiation 
    SW_Inc_b <- input_meteo$"SW_DIF_IN_Wm-2"[data_timestep] # Incoming Atmospheric Shortwave Direct Beam Radiation 
    
    SW_Out <- input_meteo$"SW_OUT_Wm-2"[data_timestep]
    LW_Out <- input_meteo$"LW_OUT_Wm-2"[data_timestep]
    
    # (I_g, I_c, I_top, I_outgoing) <- step_shortwave(SW_Inc_b, SW_Inc_d, T_soil, Timestamp)
    short_Visible <- step_shortwave(SW_Inc_b, SW_Inc_d, T_soil, Timestamp, time, 0.15, 0.10, 0.05)
    short_Infrared <- step_shortwave(SW_Inc_b, SW_Inc_d, T_soil, Timestamp, time, 0.7, 0.45, 0.25)
    
    # (L_g, L_c, L_top, L_outgoing) <- step_longwave(LW_Inc, T_soil, T_air, Timestamp)
    long <- step_longwave(LW_Inc, T_soil, T_air, Timestamp, time)
    
    ratio <- 0.8 # (Bonan, 2019)
    
    I_outgoing_predictions[data_timestep] <- ratio * short_Visible[["I_outgoing"]] + (1-ratio) * short_Infrared[["I_outgoing"]]
    L_outgoing_predictions[data_timestep] <- long[["L_outgoing"]]
    
    
    I_outgoing_data[data_timestep] <- SW_Out
    L_outgoing_data[data_timestep] <- LW_Out
    
    State_variables <<- na.omit(data.frame(I_outgoing_data, I_outgoing_predictions,
                                   L_outgoing_data, L_outgoing_predictions
                                   )) # Post hoc na.omit() need to be changed ----
    
  }
  
}






