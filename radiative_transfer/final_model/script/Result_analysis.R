## 4 ##

## Results ##

# In this script, our initial model is analysed.

## Winter days ----
# First check the power of the model during the winter day
# select 1st to 5th of January, 2016

Jan_1 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2016-01-01 01:00:00"))
Jan_5 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2016-01-05 23:00:00"))

time_range <- c(Jan_1, Jan_5)
time_start <- time_range[1]
time_end <- time_range[2]
calculate_state(time_start, time_end)

State_variables$DOY <- seq(Jan_1, Jan_5)
State_variables$Day <- input_meteo$TIMESTAMP_END[Jan_1:Jan_5]

# shortwave

ly <- expression("Shortwave outgoing radiation" ~ ("W" / "m"^2)) # ylabel
State_variables %>%
  ggplot(aes(x = Day)) +
  geom_point(aes(y = I_outgoing_predictions, color = "Prediction")) +
  geom_line(aes(y = I_outgoing_data, color = "Data")) +
  ggtitle("Winter Shortwave Outgoing Radiation") +
  labs(y = ly, x = "Date") +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.position = c(.9, .9)) +
  theme(legend.title = element_blank()) +
  theme(panel.border = element_rect(fill = NA)) +
  theme(legend.background = element_rect(# fill = "lightblue",
    colour = 1,
    alpha("grey", 0.1)))+
  scale_color_manual(values = c("#999999", "#E69F00"),
                     guide = guide_legend(override.aes = list(
                       linetype = c("solid", "blank"),
                       shape = c(NA, 16)))) 


# Overestimation

# longwave
ly <- expression("Longwave outgoing radiation" ~ ("W" / "m"^2)) # ylabel
State_variables %>%
  ggplot(aes(x = Day)) +
  geom_point(aes(y = L_outgoing_predictions, color = "Prediction")) +
  geom_line(aes(y = L_outgoing_data, color = "Data")) +
  ggtitle("Winter Longwave Outgoing Radiation") +
  labs(y = ly, x = "Date") +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.position = c(.9, .9)) +
  theme(legend.title = element_blank()) +
  theme(panel.border = element_rect(fill = NA)) +
  theme(legend.background = element_rect(# fill = "lightblue", 
    colour = 1,
    alpha("grey", 0.1)))+
  scale_color_manual(values = c("#999999", "#E69F00"),
                     guide = guide_legend(override.aes = list(
                       linetype = c("solid", "blank"),
                       shape = c(NA, 16)))) 



# Good prediction


## Summer days ----
July_1 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2016-07-01 01:00:00"))
July_5 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2016-07-05 23:00:00"))

time_range <- c(July_1, July_5)
time_start <- time_range[1]
time_end <- time_range[2]
calculate_state(time_start, time_end)

State_variables$DOY <- seq(July_1, July_5)
State_variables$Day <- input_meteo$TIMESTAMP_END[July_1:July_5]

# Shorwave
ly <- expression("Shortwave outgoing radiation" ~ ("W" / "m"^2)) # ylabel
State_variables %>%
  ggplot(aes(x = Day)) +
  geom_point(aes(y = I_outgoing_predictions, color = "Prediction")) +
  geom_line(aes(y = I_outgoing_data, color = "Data")) +
  ggtitle("Summer Shortwave Outgoing Radiation") +
  labs(y = ly, x = "Date") +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.position = c(.9, .9)) +
  theme(legend.title = element_blank()) +
  theme(panel.border = element_rect(fill = NA)) +
  theme(legend.background = element_rect(# fill = "lightblue",
    colour = 1,
    alpha("grey", 0.1)))+
  scale_color_manual(values = c("#999999", "#E69F00"),
                     guide = guide_legend(override.aes = list(
                       linetype = c("solid", "blank"),
                       shape = c(NA, 16)))) 


# Good prediction

# Longwave
ly <- expression("Longwave outgoing radiation" ~ ("W" / "m"^2)) # ylabel
State_variables %>%
  ggplot(aes(x = Day)) +
  geom_point(aes(y = L_outgoing_predictions, color = "Prediction")) +
  geom_line(aes(y = L_outgoing_data, color = "Data")) +
  ggtitle("Summer Longwave Outgoing Radiation") +
  labs(y = ly, x = "Date") +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.position = c(.9, .9)) +
  theme(legend.title = element_blank()) +
  theme(panel.border = element_rect(fill = NA)) +
  theme(legend.background = element_rect(# fill = "lightblue",
    colour = 1,
    alpha("grey", 0.1)))+
  scale_color_manual(values = c("#999999", "#E69F00"),
                     guide = guide_legend(override.aes = list(
                       linetype = c("solid", "blank"),
                       shape = c(NA, 16)))) 

# Underestimation
# Cannot predict oscillations

## All year around ----
Jan_1 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2016-01-01 01:00:00"))
Dec_31 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2016-12-31 23:00:00"))

time_range <- c(Jan_1, Dec_31)
time_start <- time_range[1]
time_end <- time_range[2]
calculate_state(time_start, time_end)

State_variables$DOY <- seq(Jan_1, Dec_31)
State_variables$Day <- input_meteo$TIMESTAMP_END[Jan_1:Dec_31]

# Shortwave
ly <- expression("Shortwave outgoing radiation" ~ ("W" / "m"^2)) # ylabel
State_variables %>%
  ggplot(aes(x = Day)) +
  geom_line(aes(y = I_outgoing_data, color = "Data"), alpha = 1 / 2) +
  geom_point(aes(y = I_outgoing_predictions, color = "Prediction"), alpha = 1 / 3) +
  ggtitle("Shortwave Outgoing Radiation") +
  labs(y = ly, x = "Date") +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.position = c(.9, .9)) +
  theme(legend.title = element_blank()) +
  theme(panel.border = element_rect(fill = NA)) +
  theme(legend.background = element_rect(# fill = "lightblue", 
    colour = 1,
    alpha("grey", 0.1)))+
  scale_color_manual(values = c("#999999", "#E69F00"),
                     guide = guide_legend(override.aes = list(
                       linetype = c("solid", "blank"),
                       shape = c(NA, 16)))) 

# tend to overestimate during the winter
# tend to underestimate during the summer

# Longwave
ly <- expression("Longwave outgoing radiation" ~ ("W" / "m"^2)) # ylabel
State_variables %>%
  ggplot(aes(x = Day)) +
  geom_line(aes(y = L_outgoing_data, color = "Data"), alpha = 1 / 2) +
  geom_point(aes(y = L_outgoing_predictions, color = "Prediction"), alpha = 1 / 3) +
  ggtitle("Longtwave Outgoing Radiation") +
  labs(y = ly, x = "Date") +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.position = c(.9, .9)) +
  theme(legend.title = element_blank()) +
  theme(panel.border = element_rect(fill = NA)) +
  theme(legend.background = element_rect(# fill = "lightblue", 
    colour = 1,
    alpha("grey", 0.1)))+
  scale_color_manual(values = c("#999999", "#E69F00"),
                     guide = guide_legend(override.aes = list(
                       linetype = c("solid", "blank"),
                       shape = c(NA, 16)))) 

# tend to underestimate during the summer

## Residuals
Jan_1_2016 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2016-01-01 01:00:00"))
Dec_31_2017 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2017-12-31 23:00:00"))

time_range <- c(Jan_1_2016, Dec_31_2017)
time_start <- time_range[1]
time_end <- time_range[2]
calculate_state(time_start, time_end)

State_variables$DOY <- seq(Jan_1_2016, Dec_31_2017)
State_variables$Day <- input_meteo$TIMESTAMP_END[Jan_1_2016:Dec_31_2017]
State_variables$I_residuals <- State_variables$I_outgoing_data - State_variables$I_outgoing_predictions
State_variables$L_residuals <- State_variables$L_outgoing_data - State_variables$L_outgoing_predictions

State_variables_2016 <- State_variables[year(State_variables$Day) == "2016", ]
State_variables_2016$Timestep <- seq(1, length(State_variables_2016$DOY))

State_variables_2017 <- State_variables[year(State_variables$Day) == "2017", ]
State_variables_2017$Timestep <- seq(1, length(State_variables_2017$DOY))

# Shortwave
ly <- expression("Residuals" ~ ("W" / "m"^2)) # ylabel

ggplot(data = State_variables_2016, aes(x = Timestep)) +
  geom_line(aes(y = I_residuals, color = "2016"), alpha = 1 / 2) +
  geom_line(data = State_variables_2017, aes(y = I_residuals, color = "2017"), alpha = 1 / 2) +
  ggtitle("Shortwave Outgoing Radiation Residuals") +
  labs(y = ly, x = "Day of the year") +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.position = c(.9, .9)) +
  theme(legend.title = element_blank()) +
  theme(panel.border = element_rect(fill = NA)) +
  theme(legend.background = element_rect(# fill = "lightblue", 
    colour = 1,
    alpha("grey", 0.1)))+
  scale_color_manual(values = c("#999999", "#E69F00"),
                     guide = guide_legend(override.aes = list(
                       linetype = c("solid", "solid")))) 


# similar trend
# The simulation during the spring improved


# Longwave
ly <- expression("Residuals" ~ ("W" / "m"^2)) # ylabel

ggplot(data = State_variables_2016, aes(x = Timestep)) +
  geom_line(aes(y = L_residuals, color = "2016"), alpha = 1 / 2) +
  geom_line(data = State_variables_2017, aes(y = L_residuals, color = "2017"), alpha = 1 / 2) +
  ggtitle("Longwave Outgoing Radiation Residuals") +
  labs(y = ly, x = "Day of the year") +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.position = c(.9, .9)) +
  theme(legend.title = element_blank()) +
  theme(panel.border = element_rect(fill = NA)) +
  theme(legend.background = element_rect(# fill = "lightblue", 
    colour = 1,
    alpha("grey", 0.1)))+
  scale_color_manual(values = c("#999999", "#E69F00"),
                     guide = guide_legend(override.aes = list(
                       linetype = c("solid", "solid")))) 

## RMSE & R2 for 2016 ----
library(Metrics)
rsq <- function (x, y) cor(x, y) ^ 2

# Shortwave
rmse(State_variables_2016$I_outgoing_data, State_variables_2016$I_outgoing_predictions)
# 11.50792 < 11.90737
rsq(State_variables_2016$I_outgoing_data, State_variables_2016$I_outgoing_predictions)
# 0.9355191 > 0.8427196

# Longwave # same
rmse(State_variables_2016$L_outgoing_data, State_variables_2016$L_outgoing_predictions)
# 15.48205
rsq(State_variables_2016$L_outgoing_data, State_variables_2016$L_outgoing_predictions)
# 0.9002458

## RMSE & R2 for 2017 ----
# Shortwave
rmse(State_variables_2017$I_outgoing_data, State_variables_2017$I_outgoing_predictions)
# 12.29437 < 12.79977
rsq(State_variables_2017$I_outgoing_data, State_variables_2017$I_outgoing_predictions)
# 0.9157956 > 0.8155532

# Longwave same
rmse(State_variables_2017$L_outgoing_data, State_variables_2017$L_outgoing_predictions)
# 13.80694
rsq(State_variables_2017$L_outgoing_data, State_variables_2017$L_outgoing_predictions)
# 0.8985186

# Overall our model predicts the observed data well.
# The accuracy of longwave outgoing radiation estimation is better.


## Manual sensitivity check for number of canopy layers ----

# N = 5
N_veg_layers = 5
N_layers <- N_veg_layers + 1
N_layers <- N_veg_layers + 1 # Total number of layers

i_bot_l <- i_soil + 1 # Index for bottom leaf layer
i_top_l <-  N_veg_layers + i_bot_l - 1 # Index for top leaf layer

Jan_1_2016 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2016-01-01 01:00:00"))
Dec_31_2017 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2017-12-31 23:00:00"))

time_range <- c(Jan_1_2016, Dec_31_2017)
time_start <- time_range[1]
time_end <- time_range[2]
calculate_state(time_start, time_end)

State_variables$DOY <- seq(Jan_1_2016, Dec_31_2017)
State_variables$Day <- input_meteo$TIMESTAMP_END[Jan_1_2016:Dec_31_2017]
State_variables$I_residuals <- State_variables$I_outgoing_data - State_variables$I_outgoing_predictions
State_variables$L_residuals <- State_variables$L_outgoing_data - State_variables$L_outgoing_predictions

State_variables_2016 <- State_variables[year(State_variables$Day) == "2016", ]
State_variables_2016$Timestep <- seq(1, length(State_variables_2016$DOY))

State_variables_2017 <- State_variables[year(State_variables$Day) == "2017", ]
State_variables_2017$Timestep <- seq(1, length(State_variables_2017$DOY))

# Shortwave
rmse(State_variables_2016$I_outgoing_data, State_variables_2016$I_outgoing_predictions)

rsq(State_variables_2016$I_outgoing_data, State_variables_2016$I_outgoing_predictions)


# Longwave 
rmse(State_variables_2016$L_outgoing_data, State_variables_2016$L_outgoing_predictions)

rsq(State_variables_2016$L_outgoing_data, State_variables_2016$L_outgoing_predictions)


## RMSE & R2 for 2017 ----
# Shortwave
rmse(State_variables_2017$I_outgoing_data, State_variables_2017$I_outgoing_predictions)

rsq(State_variables_2017$I_outgoing_data, State_variables_2017$I_outgoing_predictions)


# Longwave 
rmse(State_variables_2017$L_outgoing_data, State_variables_2017$L_outgoing_predictions)

rsq(State_variables_2017$L_outgoing_data, State_variables_2017$L_outgoing_predictions)


# N = 100
N_veg_layers = 100
N_layers <- N_veg_layers + 1
N_layers <- N_veg_layers + 1 # Total number of layers

i_bot_l <- i_soil + 1 # Index for bottom leaf layer
i_top_l <-  N_veg_layers + i_bot_l - 1 # Index for top leaf layer

Jan_1_2016 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2016-01-01 01:00:00"))
Dec_31_2017 <- which(input_meteo$TIMESTAMP_END == ymd_hms("2017-12-31 23:00:00"))

time_range <- c(Jan_1_2016, Dec_31_2017)
time_start <- time_range[1]
time_end <- time_range[2]
calculate_state(time_start, time_end)

State_variables$DOY <- seq(Jan_1_2016, Dec_31_2017)
State_variables$Day <- input_meteo$TIMESTAMP_END[Jan_1_2016:Dec_31_2017]
State_variables$I_residuals <- State_variables$I_outgoing_data - State_variables$I_outgoing_predictions
State_variables$L_residuals <- State_variables$L_outgoing_data - State_variables$L_outgoing_predictions

State_variables_2016 <- State_variables[year(State_variables$Day) == "2016", ]
State_variables_2016$Timestep <- seq(1, length(State_variables_2016$DOY))

State_variables_2017 <- State_variables[year(State_variables$Day) == "2017", ]
State_variables_2017$Timestep <- seq(1, length(State_variables_2017$DOY))

# Shortwave
rmse(State_variables_2016$I_outgoing_data, State_variables_2016$I_outgoing_predictions)
# 11.49979 < 11.7683
rsq(State_variables_2016$I_outgoing_data, State_variables_2016$I_outgoing_predictions)
# 0.9380122 > 0.8486383

# Longwave 
rmse(State_variables_2016$L_outgoing_data, State_variables_2016$L_outgoing_predictions)
# 15.48245
rsq(State_variables_2016$L_outgoing_data, State_variables_2016$L_outgoing_predictions)
# 0.9002511

## RMSE & R2 for 2017 ----
# Shortwave
rmse(State_variables_2017$I_outgoing_data, State_variables_2017$I_outgoing_predictions)
# 12.28919 < 12.65658
rsq(State_variables_2017$I_outgoing_data, State_variables_2017$I_outgoing_predictions)
# 0.9185227 > 0.8217112

# Longwave 
rmse(State_variables_2017$L_outgoing_data, State_variables_2017$L_outgoing_predictions)
# 13.80772
rsq(State_variables_2017$L_outgoing_data, State_variables_2017$L_outgoing_predictions)
# 0.8985087

# When the number is small, the prediction of shortwave goes down. 


# Taking visible and infrared into account, the predictive power for shortwave increased.
# The increase of canopy layer does not significantly change the results.

